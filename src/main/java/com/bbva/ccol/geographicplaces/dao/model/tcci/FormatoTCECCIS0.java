package com.bbva.ccol.geographicplaces.dao.model.tcci;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;


/**
 * Formato de datos <code>TCECCIS0</code> de la transacci&oacute;n <code>TCCI</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "TCECCIS0")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoTCECCIS0 {
	
	/**
	 * <p>Campo <code>CODCIUD</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "CODCIUD", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String codciud;
	
	/**
	 * <p>Campo <code>NOMCIUD</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "NOMCIUD", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String nomciud;
	
}