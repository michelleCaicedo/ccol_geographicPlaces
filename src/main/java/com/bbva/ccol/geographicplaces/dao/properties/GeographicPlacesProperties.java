package com.bbva.ccol.geographicplaces.dao.properties;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.bbva.ccol.geographicplaces.facade.v01.dto.State;
import com.bbva.ccol.geographicplaces.facade.v01.dto.StateProperties;
import com.bbva.jee.arq.spring.core.log.I18nLog;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;

@Component(value = "geographicPlacesProperties")
public class GeographicPlacesProperties {

	@Resource(name = "geographicPlacesBean")
	private Properties geographicPlacesBean;

	private static I18nLog log = I18nLogFactory.getLogI18n(
			GeographicPlacesProperties.class,
			"META-INF/spring/i18n/log/mensajesLog");

	public String getProperty(String key) {
		String propiedad = this.geographicPlacesBean.getProperty(key);
		return propiedad;
	}

	public List<StateProperties> getAllProperties() {
		log.debug("Ini lectura archivo properties");

		List<StateProperties> listAllProperties = new ArrayList<StateProperties>();

		for (int i = 1; i <= geographicPlacesBean.size(); i++) {
			StateProperties state = new StateProperties();

			String propiedad = this.geographicPlacesBean
					.getProperty("nombre.departamento." + (i));
			StringTokenizer st = new StringTokenizer(propiedad, "_");
			state.setIdIso(st.nextToken());
			state.setIdDane(st.nextToken());
			state.setName(st.nextToken());
			listAllProperties.add(state);
		}
		log.debug("Fin lectura archivo properties");
		return listAllProperties;
	}

}
