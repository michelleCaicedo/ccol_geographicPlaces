package com.bbva.ccol.geographicplaces.dao.model.tcde;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>TCDE</code>
 * 
 * @see PeticionTransaccionTcde
 * @see RespuestaTransaccionTcde
 */
@Component
public class TransaccionTcde implements InvocadorTransaccion<PeticionTransaccionTcde,RespuestaTransaccionTcde> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionTcde invocar(PeticionTransaccionTcde transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionTcde.class, RespuestaTransaccionTcde.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionTcde invocarCache(PeticionTransaccionTcde transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionTcde.class, RespuestaTransaccionTcde.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {}	
}