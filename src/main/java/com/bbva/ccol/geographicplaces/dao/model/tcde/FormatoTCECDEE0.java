package com.bbva.ccol.geographicplaces.dao.model.tcde;


import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>TCECDEE0</code> de la transacci&oacute;n <code>TCDE</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "TCECDEE0")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoTCECDEE0 {

	/**
	 * <p>Campo <code>CODDEPA</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "CODDEPA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String coddepa;
	
}