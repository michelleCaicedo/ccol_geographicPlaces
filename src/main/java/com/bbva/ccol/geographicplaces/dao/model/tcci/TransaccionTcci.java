package com.bbva.ccol.geographicplaces.dao.model.tcci;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>TCCI</code>
 * 
 * @see PeticionTransaccionTcci
 * @see RespuestaTransaccionTcci
 */
@Component
public class TransaccionTcci implements InvocadorTransaccion<PeticionTransaccionTcci,RespuestaTransaccionTcci> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionTcci invocar(PeticionTransaccionTcci transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionTcci.class, RespuestaTransaccionTcci.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionTcci invocarCache(PeticionTransaccionTcci transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionTcci.class, RespuestaTransaccionTcci.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {}	
}