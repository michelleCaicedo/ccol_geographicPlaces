package com.bbva.ccol.geographicplaces.dao;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.ccol.geographicplaces.business.dto.DTOIntCities;
import com.bbva.ccol.geographicplaces.business.dto.DTOIntState;
import com.bbva.ccol.geographicplaces.dao.model.tcci.FormatoTCECCIE0;
import com.bbva.ccol.geographicplaces.dao.model.tcci.FormatoTCECCIS0;
import com.bbva.ccol.geographicplaces.dao.model.tcci.PeticionTransaccionTcci;
import com.bbva.ccol.geographicplaces.dao.model.tcci.RespuestaTransaccionTcci;
import com.bbva.ccol.geographicplaces.dao.model.tcci.TransaccionTcci;
import com.bbva.ccol.geographicplaces.dao.model.tcde.FormatoTCECDEE0;
import com.bbva.ccol.geographicplaces.dao.model.tcde.FormatoTCECDES0;
import com.bbva.ccol.geographicplaces.dao.model.tcde.PeticionTransaccionTcde;
import com.bbva.ccol.geographicplaces.dao.model.tcde.RespuestaTransaccionTcde;
import com.bbva.ccol.geographicplaces.dao.model.tcde.TransaccionTcde;
import com.bbva.ccol.geographicplaces.dao.properties.GeographicPlacesProperties;
import com.bbva.ccol.geographicplaces.facade.v01.dto.DataCities;
import com.bbva.ccol.geographicplaces.facade.v01.mapper.Mapper;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.ErrorMappingHelper;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.jee.arq.spring.core.log.I18nLog;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;

@Component
public class GeographicPlacesDAOImpl implements GeographicPlacesDAO {

	private static I18nLog log = I18nLogFactory.getLogI18n(Mapper.class,
			"META-INF/spring/i18n/log/mensajesLog");

	@Autowired
	private TransaccionTcci tcci;
	
	@Autowired
	private TransaccionTcde tcde;

	@Autowired
	private ErrorMappingHelper emh;

	@Resource(name = "geographicPlacesProperties")
	private GeographicPlacesProperties geographicPlacesProperties;

	@Override
	public List<DTOIntState> getStates(String countryId) {
		log.debug("Ini dao getStates");
		List<DTOIntState> listDtoIntState = new ArrayList<DTOIntState>();
		PeticionTransaccionTcde peticion = new PeticionTransaccionTcde();

		CuerpoMultiparte cuerpo = new CuerpoMultiparte();
		FormatoTCECDEE0 formatoEntrada = Mapper
				.dataEntryStateToHostFormat(countryId);
		cuerpo.getPartes().add(formatoEntrada);
		peticion.setCuerpo(cuerpo);

		RespuestaTransaccionTcde respuesta = tcde.invocar(peticion);
		BusinessServiceException exception = emh
				.toBusinessServiceException(respuesta);
		if (exception != null) {
			throw exception;
		}

		List<CopySalida> copySalida = respuesta.getCuerpo().getPartes(
				CopySalida.class);
		List<FormatoTCECDES0> salida = Mapper
				.formatoFormatoUGECDES0_formatoSalidaMultiple(copySalida);
		listDtoIntState = Mapper.copyToPropertiesToIntStates(salida,
				geographicPlacesProperties.getAllProperties());
		log.debug("Fin dao getStates");
		return listDtoIntState;
	}

	@Override
	public List<DTOIntCities> getCities(DataCities dataCities) {
		log.debug("ini dao getCities");

		List<DTOIntCities> listDTOIntCities = new ArrayList<DTOIntCities>();

		PeticionTransaccionTcci peticion = new PeticionTransaccionTcci();
		CuerpoMultiparte cuerpo = new CuerpoMultiparte();
		FormatoTCECCIE0 formatoEntrada = Mapper.dataEntryCityToHostFormat(Mapper.setDtoIntToProperties(
						dataCities,
						geographicPlacesProperties.getAllProperties()));
		cuerpo.getPartes().add(formatoEntrada);
		peticion.setCuerpo(cuerpo);
		RespuestaTransaccionTcci respuesta = tcci.invocar(peticion);
		BusinessServiceException exception = emh.toBusinessServiceException(respuesta);
		if (exception != null) {
			throw exception;
		}
		List<CopySalida> copySalida = respuesta.getCuerpo().getPartes(
				CopySalida.class);
		List<FormatoTCECCIS0> salida = Mapper
				.formatoFormatoTCECCIS0_formatoSalidaMultiple(copySalida);
		listDTOIntCities = Mapper.dtoExtCitiesToDtoInt(salida, dataCities);
		log.debug("Fin dao getStates");
		return listDTOIntCities;
	}

	// TODO: method implementations

}
