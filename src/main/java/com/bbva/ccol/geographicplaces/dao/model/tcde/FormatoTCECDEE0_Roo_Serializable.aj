// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.ccol.geographicplaces.dao.model.tcde;

import java.io.Serializable;

privileged aspect FormatoTCECDEE0_Roo_Serializable {
    
    declare parents: FormatoTCECDEE0 implements Serializable;
    
    private static final long FormatoTCECDEE0.serialVersionUID = 1L;
    
}
