package com.bbva.ccol.geographicplaces.dao.model.tcci;


import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>TCECCIE0</code> de la transacci&oacute;n <code>TCCI</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "TCECCIE0")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoTCECCIE0 {

	/**
	 * <p>Campo <code>CODDEPA</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "CODDEPA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String coddepa;
	
}