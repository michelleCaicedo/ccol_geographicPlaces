// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.ccol.geographicplaces.dao.model.tcde;

import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import java.lang.String;

privileged aspect RespuestaTransaccionTcde_Roo_JavaBean {
    
    public String RespuestaTransaccionTcde.getCodigoRetorno() {
        return this.codigoRetorno;
    }
    
    public void RespuestaTransaccionTcde.setCodigoRetorno(String codigoRetorno) {
        this.codigoRetorno = codigoRetorno;
    }
    
    public String RespuestaTransaccionTcde.getCodigoControl() {
        return this.codigoControl;
    }
    
    public void RespuestaTransaccionTcde.setCodigoControl(String codigoControl) {
        this.codigoControl = codigoControl;
    }
    
    public void RespuestaTransaccionTcde.setCuerpo(CuerpoMultiparte cuerpo) {
        this.cuerpo = cuerpo;
    }
    
}
