package com.bbva.ccol.geographicplaces.dao.model.tcde;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;


/**
 * Formato de datos <code>TCECDES0</code> de la transacci&oacute;n <code>TCDE</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "TCECDES0")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoTCECDES0 {
	
	/**
	 * <p>Campo <code>CODDEPA</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "CODDEPA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String coddepa;
	
	/**
	 * <p>Campo <code>NOMDEPA</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "NOMDEPA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String nomdepa;
	
}