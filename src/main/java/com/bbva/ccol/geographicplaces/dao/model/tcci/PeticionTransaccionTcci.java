package com.bbva.ccol.geographicplaces.dao.model.tcci;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;

/**
 * <p>Transacci&oacute;n <code>TCCI</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionTcci</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionTcci</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: TCECCIE0.CCT
 * TCCIListado de Municipios               UG        TCCI-ECBVDKNPO TCECCIE0            TCCI  NS3000NNNNNN    SSTN    C   SNNSSNNN  NN                2015-05-29CE15505 2015-11-2709.55.39CICSDC112015-05-29-10.56.29.766588CE15505 0001-01-010001-01-01
 * FICHERO: TCECCIE0.FDF
 * TCECCIE0|E-FORMATO ENTRADA CONSULTA    |F|05|00034|01|00001|CODDEPA   |                 |A|002|0|R|        |
 * FICHERO: TCECCIS0.FDF
 * TCECCIS0|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|01|00001|CODCIUD    |                |A|003|0|S|        |
 * TCECCIS0|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|02|00004|NOMCIUD    |                |A|020|0|S|        |
 * FICHERO: TCECCIS0.FDX
 * TCCITCECCIE0 TCECCIE0 TCCITCECCIE0                         CE30793 2016-06-30-15.55.06.557756CE30793 2016-01-07-15.55.06.557765
 * TCCITCECCIS0 TCECCIS0 TCCITCECCIS0                         CE30793 2016-06-30-15.55.06.557756CE30793 2016-01-07-15.55.06.557765
</pre></code>
 * 
 * @see RespuestaTransaccionTcci
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "TCCI",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionTcci.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoTCECCIE0.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionTcci implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}