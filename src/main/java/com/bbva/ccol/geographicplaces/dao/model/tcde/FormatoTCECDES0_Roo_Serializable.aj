// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.ccol.geographicplaces.dao.model.tcde;

import java.io.Serializable;

privileged aspect FormatoTCECDES0_Roo_Serializable {
    
    declare parents: FormatoTCECDES0 implements Serializable;
    
    private static final long FormatoTCECDES0.serialVersionUID = 1L;
    
}
