package com.bbva.ccol.geographicplaces.facade.v01.dto;

public class DataCities {
	private String stateId;
	private String stateIdIso;
	private String stateName;
	private String countryId;
	private String cityName;
	private String pageSize;
	private String PaginationKey;

	
	
	public String getStateId() {
		return stateId;
	}
	public void setStateId(String stateId) {
		this.stateId = stateId;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getCountryId() {
		return countryId;
	}
	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getPageSize() {
		return pageSize;
	}
	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}
	public String getPaginationKey() {
		return PaginationKey;
	}
	public void setPaginationKey(String paginationKey) {
		PaginationKey = paginationKey;
	}
	public String getStateIdIso() {
		return stateIdIso;
	}
	public void setStateIdIso(String stateIdIso) {
		this.stateIdIso = stateIdIso;
	}

}
