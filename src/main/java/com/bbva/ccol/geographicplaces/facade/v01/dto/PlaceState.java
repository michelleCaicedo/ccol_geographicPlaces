
package com.bbva.ccol.geographicplaces.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "placeState", namespace = "urn:com:bbva:ccol:geographicplaces:facade:v01:dto")
@XmlType(name = "placeState", namespace = "urn:com:bbva:ccol:geographicplaces:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class PlaceState
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "ISO 3166-2 with two-digits code geographic state identifier.", required = false)
    private String id;
    @ApiModelProperty(value = "Name of the geographic state.", required = false)
    private String name;

    public PlaceState() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
