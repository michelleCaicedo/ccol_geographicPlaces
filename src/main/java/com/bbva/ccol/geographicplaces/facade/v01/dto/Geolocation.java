
package com.bbva.ccol.geographicplaces.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "geolocation", namespace = "urn:com:bbva:ccol:geographicplaces:facade:v01:dto")
@XmlType(name = "geolocation", namespace = "urn:com:bbva:ccol:geographicplaces:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Geolocation
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Geographic coordinate that indicates the north-south position of a point regarding the Equator.", required = false)
    private Double latitude;
    @ApiModelProperty(value = "Geographic coordinate that indicates the east-west position of a point regarding the Prime Meridian.", required = false)
    private Double longitude;

    public Geolocation() {
        //default constructor
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

}
