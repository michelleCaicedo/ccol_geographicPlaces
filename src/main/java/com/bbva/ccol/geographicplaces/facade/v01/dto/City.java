package com.bbva.ccol.geographicplaces.facade.v01.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "city", namespace = "urn:com:bbva:czic:geographicplaces:facade:v01:dto")
@XmlType(name = "city", namespace = "urn:com:bbva:czic:geographicplaces:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class City {
	
	public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "ISO 3166-1 two-digits country identifier.", required = false)
    private String id;
    @ApiModelProperty(value = "Name of the country.", required = false)
    private String name;
    @ApiModelProperty(value = "Geographic coordinates of a point.", required = true)
    private Geolocation geolocation;
    @ApiModelProperty(value = "Country of the geographic state", required = true)
    private Country country;
    @ApiModelProperty(value = "State of the geographic country", required = true)
    private State state;
    
    public City() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public Geolocation getGeolocation() {
        return geolocation;
    }

    public void setGeolocation(Geolocation geolocation) {
        this.geolocation = geolocation;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}
}
