package com.bbva.ccol.geographicplaces.facade.v01;

import com.bbva.ccol.geographicplaces.facade.v01.dto.DataGeographicCity;
import com.bbva.ccol.geographicplaces.facade.v01.dto.DataGeographicState;


public interface ISrvGeographicPlacesV01 {
	
 	public DataGeographicState getStates(String countryId);

	public DataGeographicCity getCities(String stateId, String countryId, String cityName,	
			String pageSize, String paginationKey);
	
}