package com.bbva.ccol.geographicplaces.facade.v01.dto;


public class StateProperties {
	private String idDane;
	private String idIso;
    private String name;
    
	public String getIdDane() {
		return idDane;
	}
	public void setIdDane(String idDane) {
		this.idDane = idDane;
	}
	public String getIdIso() {
		return idIso;
	}
	public void setIdIso(String idIso) {
		this.idIso = idIso;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
