package com.bbva.ccol.geographicplaces.facade.v01.dto;

import java.util.List;



public class DataGeographicCity {
	List<City> data;

	public List<City> getData() {
		return data;
	}

	public void setData(List<City> data) {
		this.data = data;
	}


}
