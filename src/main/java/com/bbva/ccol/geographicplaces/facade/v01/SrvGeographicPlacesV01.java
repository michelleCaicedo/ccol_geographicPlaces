package com.bbva.ccol.geographicplaces.facade.v01;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.ccol.geographicplaces.business.ISrvIntGeographicPlaces;
import com.bbva.ccol.geographicplaces.facade.v01.dto.DataCities;
import com.bbva.ccol.geographicplaces.facade.v01.dto.DataGeographicCity;
import com.bbva.ccol.geographicplaces.facade.v01.dto.DataGeographicState;
import com.bbva.ccol.geographicplaces.facade.v01.dto.State;
import com.bbva.ccol.geographicplaces.facade.v01.mapper.Mapper;
import com.bbva.jee.arq.spring.core.log.I18nLog;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;
import com.bbva.jee.arq.spring.core.servicing.annotations.SMC;
import com.bbva.jee.arq.spring.core.servicing.annotations.SN;
import com.bbva.jee.arq.spring.core.servicing.annotations.VN;
import com.bbva.jee.arq.spring.core.servicing.utils.BusinessServicesToolKit;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

@Path("/v0")
@SN(registryID = "SNCO1810002", logicalID = "geographicPlaces")
@VN(vnn = "v0")
@Api(value = "/geographicPlaces/v0", description = "This API includes any service related to the transactions of the custormes with products of another entitys.")
@Produces({ MediaType.APPLICATION_JSON })
@Service
public class SrvGeographicPlacesV01 implements ISrvGeographicPlacesV01,
		com.bbva.jee.arq.spring.core.servicing.utils.ContextAware {

	private static I18nLog log = I18nLogFactory.getLogI18n(
			SrvGeographicPlacesV01.class,
			"META-INF/spring/i18n/log/mensajesLog");

	public HttpHeaders httpHeaders;

	@Autowired
	BusinessServicesToolKit bussinesToolKit;

	public UriInfo uriInfo;

	@Override
	public void setUriInfo(UriInfo ui) {
		this.uriInfo = ui;
	}

	@Override
	public void setHttpHeaders(HttpHeaders httpHeaders) {
		this.httpHeaders = httpHeaders;
	}

	@Autowired
	ISrvIntGeographicPlaces srvIntGeographicPlaces;
	
	/**
	* Proyecto:Madiva Vivienda Online
	* Autor: Michelle Leonor Caicedo Espinel - michelleleonor.caicedo@bbva.com
	* Version: v0
	* Base URL: https://bbva-apicatalog.appspot.com/#/global/apis/data/geographicplaces
	* Git Repository: https://globaldevtools.bbva.com/bitbucket/projects/GLAPI/repos/glapi-global-apis-data-geographicplaces/browse
	**/
	@ApiOperation(value = "Service for obtaining information about the geographic states of a country.", notes = "", response = Response.class)
	@ApiResponses(value = {
			@ApiResponse(code = -1, message = "aliasGCE1"),
			@ApiResponse(code = -1, message = "aliasGCE2"),
			@ApiResponse(code = 200, message = "Found Sucessfully", response = Response.class),
			@ApiResponse(code = 500, message = "Technical Error") })
	@GET
	@Path("/states")
	@SMC(registryID = "SMCCO1810004", logicalID = "getStates")
	public DataGeographicState getStates(
			@ApiParam(value = "country.id") @DefaultValue("null") @QueryParam("country.id") String countryId) {
		DataGeographicState dataGeographicState = new DataGeographicState();
		List<State> state = new ArrayList<State>();
		state = Mapper.listDtoIntToDtoExtState(srvIntGeographicPlaces
				.getStates(countryId));
		dataGeographicState.setData(state);
		return dataGeographicState;
	}
	
	/**
	* Proyecto:Madiva Vivienda Online
	* Autor: Michelle Leonor Caicedo Espinel - michelleleonor.caicedo@bbva.com
	* Version: v0
	* Base URL: https://bbva-apicatalog.appspot.com/#/global/apis/data/geographicplaces
	* Git Repository: https://globaldevtools.bbva.com/bitbucket/projects/GLAPI/repos/glapi-global-apis-data-geographicplaces/browse
	**/
	@ApiOperation(value = "Service for managing the information associated to the geographic places defined in a country.", notes = "", response = Response.class)
	@ApiResponses(value = {
			@ApiResponse(code = -1, message = "aliasGCE1"),
			@ApiResponse(code = -1, message = "aliasGCE2"),
			@ApiResponse(code = 200, message = "Found Sucessfully", response = Response.class),
			@ApiResponse(code = 500, message = "Technical Error") })
	@GET
	@Path("/cities")
	@SMC(registryID = "SMCCO1810003", logicalID = "getCities")
	public DataGeographicCity getCities(
			@ApiParam(value = "state.id") @DefaultValue("null") @QueryParam("state.id") String stateId,
			@ApiParam(value = "country.id") @DefaultValue("null") @QueryParam("country.id") String countryId,
			@ApiParam(value = "city.name") @DefaultValue("null") @QueryParam("city.name") String cityName,
			@ApiParam(value = "pageSize") @DefaultValue("null") @QueryParam("pageSize") String pageSize,
			@ApiParam(value = "paginationKey") @DefaultValue("null") @QueryParam("paginationKey") String paginationKey) {
		DataGeographicCity dataGeographicCity = new DataGeographicCity();
		DataCities dataCities = Mapper.entryDataCities(stateId, countryId,
				cityName, pageSize, paginationKey);
		dataGeographicCity.setData(Mapper.listDtoIntToDtoExtCity(srvIntGeographicPlaces.getCities(dataCities)));
		return dataGeographicCity;
	}

}
