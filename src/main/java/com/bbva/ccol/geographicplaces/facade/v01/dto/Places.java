
package com.bbva.ccol.geographicplaces.facade.v01.dto;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "places", namespace = "urn:com:bbva:ccol:geographicplaces:facade:v01:dto")
@XmlType(name = "places", namespace = "urn:com:bbva:ccol:geographicplaces:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Places
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Locality identifier.", required = false)
    private String id;
    @ApiModelProperty(value = "Name of the geographic place.", required = false)
    private String name;
    @ApiModelProperty(value = "Geographic coordinates of a point.", required = true)
    private Geolocation geolocation;
    @ApiModelProperty(value = "Postal code related to a geographic area (a locality, for example).", required = false)
    private String zipCode;
    @ApiModelProperty(value = "Geographic place type:It is a representation of the possible location data, identified for a geographic area, where more than one type is considered, that is, a colony or a delegation can be returned, for a more complete description of the place.", required = false)
    private List<GeographicPlaceTypes> geographicPlaceTypes;
    @ApiModelProperty(value = "Information of the city related to this geographic place.", required = false)
    private PlaceCity city;
    @ApiModelProperty(value = "Postal code related to a geographic area (a locality, for example).", required = false)
    private PlaceState state;
    @ApiModelProperty(value = "Information of the state related to this geographic place.", required = false)
    private Country country;

    public Places() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Geolocation getGeolocation() {
        return geolocation;
    }

    public void setGeolocation(Geolocation geolocation) {
        this.geolocation = geolocation;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public List<GeographicPlaceTypes> getGeographicPlaceTypes() {
        return geographicPlaceTypes;
    }

    public void setGeographicPlaceTypes(List<GeographicPlaceTypes> geographicPlaceTypes) {
        this.geographicPlaceTypes = geographicPlaceTypes;
    }

    public PlaceCity getCity() {
        return city;
    }

    public void setCity(PlaceCity city) {
        this.city = city;
    }

    public PlaceState getState() {
        return state;
    }

    public void setState(PlaceState state) {
        this.state = state;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

}
