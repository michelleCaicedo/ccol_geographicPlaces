package com.bbva.ccol.geographicplaces.facade.v01.dto;

import java.util.List;



public class DataGeographicState {
	List<State> data;

	public List<State> getData() {
		return data;
	}

	public void setData(List<State> data) {
		this.data = data;
	}


}
