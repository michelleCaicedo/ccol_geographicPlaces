package com.bbva.ccol.geographicplaces.facade.v01.mapper;

import java.util.ArrayList;
import java.util.List;

import com.bbva.ccol.geographicplaces.business.dto.DTOIntCities;
import com.bbva.ccol.geographicplaces.business.dto.DTOIntCountry;
import com.bbva.ccol.geographicplaces.business.dto.DTOIntGeolocation;
import com.bbva.ccol.geographicplaces.business.dto.DTOIntState;
import com.bbva.ccol.geographicplaces.business.dto.DTOIntStatesMadiva;
import com.bbva.ccol.geographicplaces.dao.model.tcci.FormatoTCECCIE0;
import com.bbva.ccol.geographicplaces.dao.model.tcci.FormatoTCECCIS0;
import com.bbva.ccol.geographicplaces.dao.model.tcde.FormatoTCECDEE0;
import com.bbva.ccol.geographicplaces.dao.model.tcde.FormatoTCECDES0;
import com.bbva.ccol.geographicplaces.facade.v01.dto.City;
import com.bbva.ccol.geographicplaces.facade.v01.dto.DataCities;
import com.bbva.ccol.geographicplaces.facade.v01.dto.State;
import com.bbva.ccol.geographicplaces.facade.v01.dto.StateProperties;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.jee.arq.spring.core.log.I18nLog;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Mapper {

	private static I18nLog log = I18nLogFactory.getLogI18n(Mapper.class,
			"META-INF/spring/i18n/log/mensajesLog");

	public static List<DTOIntState> jsonToDtoIntStatesMap(String stateJson) {
		List<DTOIntStatesMadiva> listDtoIntStatesMadiva = new ArrayList<DTOIntStatesMadiva>();
		List<DTOIntState> listDtoIntState = new ArrayList<DTOIntState>();
		DTOIntState dtoIntState;
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(
				DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		try {
			listDtoIntStatesMadiva = objectMapper.readValue(stateJson,
					new TypeReference<List<DTOIntStatesMadiva>>() {
					});
		} catch (Exception e) {
			throw new BusinessServiceException(e.getMessage() + stateJson);
		}
		for (DTOIntStatesMadiva listStateMadiva : listDtoIntStatesMadiva) {
			dtoIntState = new DTOIntState();
			dtoIntState.setId(listStateMadiva.getIdProvinciaINE().toString());
			dtoIntState.setName(listStateMadiva.getNombreProvincia().toString());
			DTOIntGeolocation geolocation = new DTOIntGeolocation();
			geolocation.setLatitude(listStateMadiva.getLatitud());
			geolocation.setLongitude(listStateMadiva.getLongitud());
			dtoIntState.setGeolocation(geolocation);
			listDtoIntState.add(dtoIntState);
		}

		if (listDtoIntState == null)
			throw new BusinessServiceException("wrongParameters");

		return listDtoIntState;
	}
	
		public static List<DTOIntState> copyToPropertiesToIntStates(
			List<FormatoTCECDES0> listFormat,
			List<StateProperties> listStateProperties) {
		log.debug("Ini mapper copyToPropertiesToIntStates");
		List<DTOIntState> listDTOIntState = new ArrayList<DTOIntState>();
		DTOIntCountry dtoIntCountry = new DTOIntCountry();
		dtoIntCountry.setId("COL");
		dtoIntCountry.setName("COLOMBIA");
		for (StateProperties listFormatDistri : listStateProperties) {
			DTOIntState dtoIntState = new DTOIntState();
			for (int i = 0; i <= listFormat.size() - 1; i++) {
				if (listFormatDistri.getIdDane().trim().equalsIgnoreCase(listFormat.get(i).getCoddepa().trim())) {
					dtoIntState.setName(listFormatDistri.getName());
					dtoIntState.setId(listFormatDistri.getIdIso());
					dtoIntState.setCountry(dtoIntCountry);
					listDTOIntState.add(dtoIntState);
					listFormat.remove(i);
					i = 0;
					break;
				}
			}
		}
		log.debug("Fin mapper copyToPropertiesToIntStates");
		return listDTOIntState;
	}
	
		public static List<State> listDtoIntToDtoExtState(
			List<DTOIntState> dtoIntState) {

		List<State> listState = new ArrayList<State>();
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(
				DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		try {
			String jsonString = objectMapper.writeValueAsString(dtoIntState);
			listState = objectMapper.readValue(jsonString,new TypeReference<List<State>>() {
					});
		} catch (Exception e) {
			log.error("Error mapeo dtoInt to dto");
		}
		return listState;
	}
	
		public static List<FormatoTCECDES0> formatoFormatoUGECDES0_formatoSalidaMultiple(
			List<CopySalida> copySalida) {
		log.debug("Ini mapper formatoFormatoUGECDES0_formatoSalidaMultiple");
		ArrayList<FormatoTCECDES0> formatos = new ArrayList<FormatoTCECDES0>();
		for (CopySalida cs : copySalida) {
			FormatoTCECDES0 f = cs.getCopy(FormatoTCECDES0.class);
			if (f != null) {
				formatos.add(f);
			}
		}
		log.debug("Fin mapper formatoFormatoUGECDES0_formatoSalidaMultiple");
		return formatos;
	}

	public static FormatoTCECDEE0 dataEntryStateToHostFormat(String countryId) {
		FormatoTCECDEE0 formatoEntrada = new FormatoTCECDEE0();
		formatoEntrada.setPais(countryId);
		log.debug("Fin de mappeos entrada");
		return formatoEntrada;
	}
	
	public static DataCities setDtoIntToProperties(DataCities dataCities,
			List<StateProperties> listState) {
		log.debug("Ini mapper setDtoIntToProperties");
		for (StateProperties listFormatDistri : listState) {
			if (listFormatDistri.getIdIso().equalsIgnoreCase(
					dataCities.getStateId())) {
				dataCities.setStateId(listFormatDistri.getIdDane());
				dataCities.setStateIdIso(listFormatDistri.getIdIso());
				dataCities.setStateName(listFormatDistri.getName());
			}
		}
		log.debug("Fin mapper setDtoIntToProperties");
		return dataCities;
	}

	public static List<City> listDtoIntToDtoExtCity(
			List<DTOIntCities> dtoIntCity) {
		List<City> listCity = new ArrayList<City>();
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(
				DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		try {
			String jsonString = objectMapper.writeValueAsString(dtoIntCity);
			listCity = objectMapper.readValue(jsonString,
					new TypeReference<List<City>>() {
					});
		} catch (Exception e) {
			log.error("Error mapeo listDtoIntToDtoExtCity "+e);
		}
		return listCity;
	}

	public static DataCities entryDataCities(String stateId, String countryId,
			String cityName, String pageSize, String paginationKey) {
		DataCities dataCities = new DataCities();

		if (validateNullOrEmpty(stateId))
			dataCities.setStateId(stateId);

		if (validateNullOrEmpty(countryId))
			dataCities.setCountryId(countryId);

		if (validateNullOrEmpty(cityName))
			dataCities.setCityName(cityName);

		if (validateNullOrEmpty(pageSize))
			dataCities.setPageSize(pageSize);

		if (validateNullOrEmpty(paginationKey))
			dataCities.setPaginationKey(paginationKey);
		return dataCities;
	}

	public static boolean validateNullOrEmpty(String palabra) {
		return (palabra != null && !palabra.equalsIgnoreCase(""));
	}

	public static FormatoTCECCIE0 dataEntryCityToHostFormat(
			DataCities dataCities) {
		FormatoTCECCIE0 formatoEntrada = new FormatoTCECCIE0();
		formatoEntrada.setCoddepa(dataCities.getStateId());
		log.debug("Fin de mappeos entrada");
		return formatoEntrada;
	}
	
	public static CopySalida copy_list(List<Object> respuesta) {
		List<CopySalida> listResponseForm = new ArrayList<CopySalida>();
		CopySalida formatoSalida = new CopySalida();
		for (Object responseObject : respuesta) {

			if ((CopySalida) responseObject != null) {
				formatoSalida = (CopySalida) responseObject;
				listResponseForm.add(formatoSalida);
			}
		}
		return formatoSalida;
	}

	public static List<FormatoTCECCIS0> formatoFormatoTCECCIS0_formatoSalidaMultiple(
			List<CopySalida> copySalida) {
		log.debug("Ini mapper formatoFormatoTCECCIS0_formatoSalidaMultiple");
		ArrayList<FormatoTCECCIS0> formatos = new ArrayList<FormatoTCECCIS0>();
		for (CopySalida cs : copySalida) {
			FormatoTCECCIS0 f = cs.getCopy(FormatoTCECCIS0.class);
			if (f != null) {
				formatos.add(f);
			}
		}
		log.debug("Fin mapper formatoFormatoTCECCIS0_formatoSalidaMultiple");
		return formatos;
	}

	public static List<DTOIntCities> dtoExtCitiesToDtoInt(
			List<FormatoTCECCIS0> listFormatoTCECCIS0, DataCities dataCities) {
		List<DTOIntCities> listDTOIntCities = new ArrayList<DTOIntCities>();
		DTOIntCountry country = new DTOIntCountry();
		DTOIntState dtoIntState = new DTOIntState();
		country.setId("CO");
		country.setName("COLOMBIA");
		dtoIntState.setId(dataCities.getStateIdIso());
		dtoIntState.setName(dataCities.getStateName());
		for (FormatoTCECCIS0 formatoTCECCIS0 : listFormatoTCECCIS0) {
			DTOIntCities dtoIntCities = new DTOIntCities();
			dtoIntCities.setId(formatoTCECCIS0.getCodciud());
			dtoIntCities.setName(formatoTCECCIS0.getNomciud());
			dtoIntCities.setCountry(country);
			dtoIntCities.setState(dtoIntState);
			listDTOIntCities.add(dtoIntCities);
		}

		return listDTOIntCities;
	}

}
