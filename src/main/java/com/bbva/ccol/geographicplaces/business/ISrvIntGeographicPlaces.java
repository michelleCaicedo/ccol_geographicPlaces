package com.bbva.ccol.geographicplaces.business;

import java.util.List;

import com.bbva.ccol.geographicplaces.business.dto.DTOIntCities;
import com.bbva.ccol.geographicplaces.business.dto.DTOIntState;
import com.bbva.ccol.geographicplaces.facade.v01.dto.DataCities;



public interface ISrvIntGeographicPlaces {
 	public List<DTOIntState> getStates(String countryId);

	public List<DTOIntCities> getCities(DataCities dataCities);

	
}