package com.bbva.ccol.geographicplaces.business.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.ccol.geographicplaces.business.ISrvIntGeographicPlaces;
import com.bbva.ccol.geographicplaces.business.dto.DTOIntCities;
import com.bbva.ccol.geographicplaces.business.dto.DTOIntPlaces;
import com.bbva.ccol.geographicplaces.business.dto.DTOIntState;
import com.bbva.ccol.geographicplaces.dao.GeographicPlacesDAO;
import com.bbva.ccol.geographicplaces.facade.v01.dto.DataCities;
import com.bbva.jee.arq.spring.core.log.I18nLog;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;
import com.bbva.jee.arq.spring.core.servicing.utils.BusinessServicesToolKit;


@Service
public class SrvIntGeographicPlaces implements ISrvIntGeographicPlaces {
	
	private static I18nLog log = I18nLogFactory.getLogI18n(SrvIntGeographicPlaces.class,"META-INF/spring/i18n/log/mensajesLog");


	@Autowired
	BusinessServicesToolKit bussinesToolKit;
	
	@Autowired
	GeographicPlacesDAO dao;
	
	@Override
	public List<DTOIntState> getStates(String countryId) {
		return dao.getStates(countryId);
	}

		
	@Override
	public List<DTOIntCities> getCities(DataCities dataCities) {
		return dao.getCities(dataCities);
	}

	

}
