package com.bbva.ccol.geographicplaces.business.dto;

public class DTOIntStatesMadiva {
	
	private Long idProvinciaINE;
	private String nombreProvincia;
	private Double longitud;
	private Double latitud;
	private Long idProvinciaMEH;
	private String geoJson;
	

	public Long getIdProvinciaINE() {
		return idProvinciaINE;
	}
	public void setIdProvinciaINE(Long idProvinciaINE) {
		this.idProvinciaINE = idProvinciaINE;
	}
	public Long getIdProvinciaMEH() {
		return idProvinciaMEH;
	}
	public void setIdProvinciaMEH(Long idProvinciaMEH) {
		this.idProvinciaMEH = idProvinciaMEH;
	}
	public Double getLongitud() {
		return longitud;
	}
	public void setLongitud(Double longitud) {
		this.longitud = longitud;
	}
	public Double getLatitud() {
		return latitud;
	}
	public void setLatitud(Double latitud) {
		this.latitud = latitud;
	}
	public String getNombreProvincia() {
		return nombreProvincia;
	}
	public void setNombreProvincia(String nombreProvincia) {
		this.nombreProvincia = nombreProvincia;
	}
	public String getGeoJson() {
		return geoJson;
	}
	public void setGeoJson(String geoJson) {
		this.geoJson = geoJson;
	}
}