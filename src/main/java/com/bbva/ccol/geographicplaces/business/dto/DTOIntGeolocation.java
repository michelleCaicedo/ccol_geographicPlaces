
package com.bbva.ccol.geographicplaces.business.dto;




public class DTOIntGeolocation {

    public final static long serialVersionUID = 1L;
    private Double latitude;
    private Double longitude;

    public DTOIntGeolocation() {
        //default constructor
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

}
