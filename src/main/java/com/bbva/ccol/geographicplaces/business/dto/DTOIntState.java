
package com.bbva.ccol.geographicplaces.business.dto;




public class DTOIntState {

    public final static long serialVersionUID = 1L;
    private String id;
    private String name;
    private DTOIntGeolocation geolocation;
    private DTOIntCountry country;

    public DTOIntState() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DTOIntGeolocation getGeolocation() {
        return geolocation;
    }

    public void setGeolocation(DTOIntGeolocation geolocation) {
        this.geolocation = geolocation;
    }

    public DTOIntCountry getCountry() {
        return country;
    }

    public void setCountry(DTOIntCountry country) {
        this.country = country;
    }

}
