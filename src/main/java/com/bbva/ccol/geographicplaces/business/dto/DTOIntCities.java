
package com.bbva.ccol.geographicplaces.business.dto;




public class DTOIntCities {

    public final static long serialVersionUID = 1L;
    private String id;
    private String name;
    private DTOIntGeolocation geolocation;
    private DTOIntCountry country;
    private DTOIntState state;

    public DTOIntCities() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DTOIntGeolocation getGeolocation() {
        return geolocation;
    }

    public void setGeolocation(DTOIntGeolocation geolocation) {
        this.geolocation = geolocation;
    }


    public DTOIntState getState() {
        return state;
    }

    public void setState(DTOIntState state) {
        this.state = state;
    }

    public DTOIntCountry getCountry() {
        return country;
    }

    public void setCountry(DTOIntCountry country) {
        this.country = country;
    }

}
