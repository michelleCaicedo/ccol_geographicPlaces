
package com.bbva.ccol.geographicplaces.business.dto;

import java.util.List;



public class DTOIntPlaces {

    public final static long serialVersionUID = 1L;
    private String id;
    private String name;
    private DTOIntGeolocation geolocation;
    private String zipCode;
    private List<DTOIntGeographicPlaceTypes> geographicPlaceTypes;
    private DTOIntPlaceCity city;
    private DTOIntPlaceState state;
    private DTOIntCountry country;

    public DTOIntPlaces() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DTOIntGeolocation getGeolocation() {
        return geolocation;
    }

    public void setGeolocation(DTOIntGeolocation geolocation) {
        this.geolocation = geolocation;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public List<DTOIntGeographicPlaceTypes> getGeographicPlaceTypes() {
        return geographicPlaceTypes;
    }

    public void setGeographicPlaceTypes(List<DTOIntGeographicPlaceTypes> geographicPlaceTypes) {
        this.geographicPlaceTypes = geographicPlaceTypes;
    }

    public DTOIntPlaceCity getCity() {
        return city;
    }

    public void setCity(DTOIntPlaceCity city) {
        this.city = city;
    }

    public DTOIntPlaceState getState() {
        return state;
    }

    public void setState(DTOIntPlaceState state) {
        this.state = state;
    }

    public DTOIntCountry getCountry() {
        return country;
    }

    public void setCountry(DTOIntCountry country) {
        this.country = country;
    }

}
